//
//  TeamView.swift
//  NLRecords2017
//
//  Created by Chris Peragine on 1/14/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import UIKit

class TeamView: UIViewController {

    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var wins: UILabel!
    @IBOutlet weak var loss: UILabel!
    @IBOutlet weak var titles: UILabel!
    
    var team: Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTeam()
    }
    
    func setTeam() {
        teamLogo.image = team?.image
        teamName.text = team?.team
        wins.text = "\(team?.win ?? 0)"
        loss.text = "\(team?.loss ?? 0)"
        titles.text = "\(team?.titles ?? 0)"
        
    }

}
