//
//  NLRecordsView.swift
//  NLRecords2017
//
//  Created by Chris Peragine on 1/14/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import UIKit

class NLRecordsView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    //Declare array as typed so you can call methods for iter
    private var teams: [Team] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addNavImage()
        teams = Team.createTeams()
        sortData()

    }
    
    func addNavImage() {
        let navController = navigationController!
        
        let image = #imageLiteral(resourceName: "nl")
        let imageView = UIImageView(image: image)
        
        ///make my life easier
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - image.size.width / 2
        let bannerY = bannerHeight / 2 - image.size.height / 2
        
        //size assignment
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        
        //make nav titleView = this imageview
        navigationItem.titleView = imageView
    }
    
    func sortData() {
        /*enum for sections
        enum TableSection: Int {
            case action = 0, central, west, total
        }
        
        //Data variable/tuple to track sorted data
        var data = [TableSection: [Team]]()
        
        //data[.east] = teams.filter({ teams.contains(where: $0.division == "East") })
        */
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TeamViewSeg" {
            let destination = segue.destination as! TeamView
            destination.team = sender as? Team
        }
    }
}

extension NLRecordsView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        if (section == 0) {
            label.text = "  East"
        } else if (section == 1) {
            label.text = "  Central"
        } else {
            label.text = "  West"
        }
        label.textColor = UIColor.white
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        label.backgroundColor = UIColor.red
        return label
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    //If I have time, come back and set up as enum/sorted/rawvalues, put AL in data, same for section headers, etc.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
        var team = teams[indexPath.row]
        if (indexPath.section == 1) {
            team = teams[indexPath.row + 5]
        } else if (indexPath.section == 2) {
            team = teams[indexPath.row + 10]
        }
        cell.setTeam(team: team)
        return cell
    }
    
    //Remove row selection highlighting and segueperf
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var team = teams[indexPath.row]
        if (indexPath.section == 1) {
            team = teams[indexPath.row + 5]
        } else if (indexPath.section == 2) {
            team = teams[indexPath.row + 10]
        }
        performSegue(withIdentifier: "TeamViewSeg", sender: team)
 
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}











