//
//  TeamCell.swift
//  NLRecords2017
//
//  Created by Chris Peragine on 1/14/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {

    @IBOutlet weak var teamImgView: UIImageView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var recordLabel: UILabel!
    
    func setTeam(team: Team) {
        teamImgView.image = team.image
        teamNameLabel.text = team.team
        recordLabel.text = "\(team.win)-\(team.loss)"
        
    }
    
    

}
