//
//  team.swift
//  NLRecords2017
//
//  Created by Chris Peragine on 1/14/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//

import Foundation
import UIKit

class Team {
    
    var team: String
    var image: UIImage
    var league: String
    var division: String
    var win: Int
    var loss: Int
    var titles: Int
    
    // Initialize stored properties
    init(team: String, image: UIImage, league: String, division: String, win: Int, loss: Int, titles: Int) {
        self.team = team
        self.image = image
        self.league = league
        self.division = division
        self.win = win
        self.loss = loss
        self.titles = titles
    }
    
    class func createTeams() -> [Team]{
        let team = [
            Team(team: "Washington Nationals", image: #imageLiteral(resourceName: "nationals"), league: "National", division: "East", win: 97, loss: 65, titles: 0),
            Team(team: "Miami Marlins", image: #imageLiteral(resourceName: "marlins"), league: "National", division: "East", win: 77, loss: 85, titles: 2),
            Team(team: "Atlanta Braves", image: #imageLiteral(resourceName: "braves"), league: "National", division: "East", win: 72, loss: 90, titles: 3),
            Team(team: "New York Mets", image: #imageLiteral(resourceName: "mets"), league: "National", division: "East", win: 70, loss: 92, titles: 2),
            Team(team: "Philadelphia Philthies", image: #imageLiteral(resourceName: "philthies"), league: "National", division: "East", win: 66, loss: 96, titles: 2),
            Team(team: "Chicago Cubs", image: #imageLiteral(resourceName: "cubs"), league: "National", division: "Central", win: 92, loss: 70, titles: 3),
            Team(team: "Milwaukee Brewers", image: #imageLiteral(resourceName: "brewers"), league: "National", division: "Central", win: 86, loss: 76, titles: 0),
            Team(team: "St. Louis Cardinals", image: #imageLiteral(resourceName: "cardinals"), league: "National", division: "Central", win: 83, loss: 79, titles: 11),
            Team(team: "Pittsburgh Pirates", image: #imageLiteral(resourceName: "pirates"), league: "National", division: "Central", win: 75, loss: 87, titles: 5),
            Team(team: "Cincinnati Reds", image: #imageLiteral(resourceName: "reds"), league: "National", division: "Central", win: 68, loss: 94, titles: 5),
            Team(team: "Los Angeles Dodgers", image: #imageLiteral(resourceName: "dodgers"), league: "National", division: "West", win: 104, loss: 58, titles: 6),
            Team(team: "Arizona Diamondbacks", image: #imageLiteral(resourceName: "dbacks"), league: "National", division: "West", win: 93, loss: 69, titles: 1),
            Team(team: "Colorado Rockies", image: #imageLiteral(resourceName: "rockies"), league: "National", division: "West", win: 87, loss: 75, titles: 0),
            Team(team: "San Diego Padres", image: #imageLiteral(resourceName: "padres"), league: "National", division: "West", win: 71, loss: 91, titles: 0),
            Team(team: "San Francisco Giants", image: #imageLiteral(resourceName: "giants"), league: "National", division: "West", win: 64, loss: 98, titles: 8)
        ]
        return team
    }
}

